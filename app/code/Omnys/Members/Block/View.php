<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Omnys\Members\Block;

class View extends \Magento\Framework\View\Element\Template
{
    protected $_customerFactory;
    protected $_context;
    protected $_data;

    public function __construct(
            \Magento\Customer\Model\CustomerFactory $customerFactory,
            \Magento\Framework\View\Element\Template\Context $context,
            array $data = array()) {
        $this->_customerFactory = $customerFactory;
        $this->_context = $context;
        $this->_data = $data;
        parent::__construct($context, $data);
    }

    public function getOne() {
        return "one";
    }

    public function getCustomerCollection() {
        $customerCollection = $this->_customerFactory->create();

        return $customerCollection->getCollection();
    }

    public function getCustomerByEmail($email) {
        $customer = $this->_customerFactory->create();
        $customer->setWebsiteId(1);
        $customerInfo = $customer->loadByEmail($email);

        if ($customerInfo) {
            return $customerInfo;
        }

        return null;
    }
}
