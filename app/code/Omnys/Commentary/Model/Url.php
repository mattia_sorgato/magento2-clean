<?php

namespace Omnys\Commentary\Model;

use Magento\Framework\UrlInterface;

/**
 * Commentary Url class, necessary for frontend commentary edit
 *
 * @author mattia
 */
class Url {
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @param UrlInterface $urlBuilder
     */
    public function __construct(
        UrlInterface $urlBuilder
    ) {
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * Retrieve customer register form url
     *
     * @return string
     */
    public function getRegisterUrl()
    {
        return $this->urlBuilder->getUrl('commentary/commentary/create');
    }
}
