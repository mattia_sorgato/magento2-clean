<?php
/**
 * Copyright © 2016 AionNext Ltd. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Omnys\Commentary\Model\ResourceModel;

/**
 * Aion Test resource model
 */
class Commentary extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('omnys_commentary', 'commentary_id');
    }

}