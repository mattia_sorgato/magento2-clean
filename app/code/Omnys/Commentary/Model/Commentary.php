<?php

namespace Omnys\Commentary\Model;

/**
 * Omnys Commentary model
 *
 * @method \Omnys\Commentary\Model\ResourceModel\Commentary _getResource()
 * @method \Omnys\Commentary\Model\ResourceModel\Commentary getResource()
 * @method string getId()
 * @method string getName()
 * @method string getEmail()
 * @method setSortOrder()
 * @method int getSortOrder()
 */
class Commentary extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Aion Test cache tag
     */
    const CACHE_TAG = 'omnys_commentary';

    /**
     * @var string
     */
    protected $_cacheTag = 'omnys_commentary';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'omnys_commentary';

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Omnys\Commentary\Model\ResourceModel\Commentary');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId(), self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Prepare item's statuses
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

}