<?php

namespace Omnys\Commentary\Setup;

use Omnys\Commentary\Model\Commentary;
use Omnys\Commentary\Model\CommentaryFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * Commentary factory
     *
     * @var CommentaryFactory
     */
    private $commentaryFactory;

    /**
     * Init
     *
     * @param TestFactory $testFactory
     */
    public function __construct(CommentaryFactory $commentaryFactory)
    {
        $this->commentaryFactory = $commentaryFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $testItems = [
            [
                'title' => 'John Doe knows best',
                'comment' => 'really cool Magento 2 installation, bro! (bumpfist)',
                'username' => 'JohnDoeRocks',
                'email' => 'john.doe@example.com',
                'is_active' => 1,
            ],
            [
                'title' => 'John Doe says right',
                'comment' => 'I think that too!',
                'username' => 'TheLady',
                'email' => 'jane.doe@example.com',
                'is_active' => 0,
            ],

            [
                'title' => 'Short and focused title',
                'comment' => 'Long description of personal opinion, containig IMHO everywhere',
                'username' => 'NotABot',
                'email' => 'steve.test@example.com',
                'is_active' => 1,
            ],
        ];

        /**
         * Insert default items
         */
        foreach ($testItems as $data) {
            $this->createCommentary()->setData($data)->save();
        }

        $setup->endSetup();
    }

    /**
     * Create Commentary item
     *
     * @return Commentary
     */
    public function createCommentary()
    {
        return $this->commentaryFactory->create();
    }
}
