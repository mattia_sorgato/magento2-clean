<?php

namespace Omnys\Commentary\Block\Adminhtml;

/**
 * Adminhtml Commentary items content block
 */
class Commentary extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_blockGroup = 'Omnys_Commentary';
        $this->_controller = 'adminhtml_commentary';
        $this->_headerText = __('Commentary Items');
        $this->_addButtonLabel = __('Add New Item');
        parent::_construct();
    }
}