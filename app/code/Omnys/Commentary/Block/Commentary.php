<?php

namespace Omnys\Commentary\Block;

use Magento\Framework\View\Element\Template;

/**
 * Omnys Commentary Page block
 */
class Commentary extends Template
{
    /**
     * @var \Omnys\Commentary\Model\Commentary
     */
    protected $commentary;

    /**
     * Commentary factory
     *
     * @var \Omnys\Commentary\Model\CommentaryFactory
     */
    protected $commentaryFactory;

    /**
     * @var \Omnys\Commentary\Model\ResourceModel\Commentary\CollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var \Omnys\Commentary\Model\ResourceModel\Commentary\Collection
     */
    protected $items;

    /**
     * @var \Omnys\Commentary\Model\Url
     */
    protected $_commentaryUrl;

    /**
     * Commentary constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Omnys\Commentary\Model\Commentary $commentary
     * @param \Omnys\Commentary\Model\CommentaryFactory $commentaryFactory
     * @param \Omnys\Commentary\Model\Url $commentaryUrl
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Omnys\Commentary\Model\Commentary $commentary,
        \Omnys\Commentary\Model\CommentaryFactory $commentaryFactory,
        \Omnys\Commentary\Model\ResourceModel\Commentary\CollectionFactory $itemCollectionFactory,
        \Omnys\Commentary\Model\Url $commentaryUrl,
        array $data = []
    ) {
        $this->commentary = $commentary;
        $this->commentaryFactory = $commentaryFactory;
        $this->itemCollectionFactory = $itemCollectionFactory;
        $this->_commentaryUrl = $commentaryUrl;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve Commentary instance
     *
     * @return \Omnys\Commentary\Model\Commentary
     */
    public function getCommentaryModel()
    {
        if (!$this->hasData('commentary')) {
            if ($this->getCommentaryId()) {
                /** @var \Omnys\Commentary\Model\Commentary $commentary */
                $commentary = $this->commentaryFactory->create();
                $commentary->load($this->getCommentaryId());
            } else {
                $commentary = $this->commentary;
            }
            $this->setData('commentary', $commentary);
        }
        return $this->getData('commentary');
    }

    /**
     * Get items
     *
     * @return bool | \Omnys\Commentary\ResourceModel\Commentary\Collection
     */
    public function getItems()
    {
        if (!$this->items) {
            $this->items = $this->itemCollectionFactory->create()->addFieldToSelect(
                '*'
            )->addFieldToFilter(
                'is_active',
                ['eq' => \Omnys\Commentary\Model\Commentary::STATUS_ENABLED]
            )->setOrder(
                'creation_time',
                'desc'
            );
        }
        return $this->items;
    }

    /**
     * Get Commentary Id
     *
     * @return int
     */
    public function getCommentaryId()
    {
        return 1;
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Omnys\Commentary\Model\Commentary::CACHE_TAG . '_' . $this->getCommentaryModel()->getId()];
    }

    /**
     * Test function
     *
     * @return string
     */
    public function getTest()
    {
        return 'This is a test function for some logic...';
    }

    /**
     *
     */
    public function getPostActionUrl()
    {
        return $this->_commentaryUrl->getRegisterUrl();
    }
}

