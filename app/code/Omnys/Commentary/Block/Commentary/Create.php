<?php

namespace Omnys\Commentary\Block\Commentary;

use Magento\Framework\View\Element\Template;

/**
 * Omnys Commentary Creation Page block
 */
class Create extends Template
{

    /**
     * Commentary factory
     *
     * @var \Omnys\Commentary\Model\CommentaryFactory
     */
    protected $commentaryFactory;

    /**
     * @var \Omnys\Commentary\Model\ResourceModel\Commentary\CollectionFactory
     */
    protected $itemCollectionFactory;

    /**
     * @var \Omnys\Commentary\Model\ResourceModel\Commentary\Collection
     */
    protected $items;

    /**
     * Commentary create constructor.
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Omnys\Commentary\Model\CommentaryFactory $commentaryFactory
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Omnys\Commentary\Model\CommentaryFactory $commentaryFactory,
        array $data = []
    ) {
        $this->commentaryFactory = $commentaryFactory;
        parent::__construct($context, $data);
    }

    /**
     * Return identifiers for produced content
     *
     * @return array
     */
    public function getIdentities()
    {
        return [\Omnys\Commentary\Model\Commentary::CACHE_TAG . '_' . $this->getCommentaryModel()->getId()];
    }

    /**
     * Get back Url in commentary creation.
     *
     * @return string
     */
    public function getBackUrl()
    {
        // the RefererUrl must be set in appropriate controller
        if ($this->getRefererUrl()) {
            return $this->getRefererUrl();
        }
        return $this->getUrl('commentary/');
    }

    /**
     * Get the commentary just created
     *
     * @return \Omnys\Commentary\Model\Commentary
     */
    public function getNewCommentary()
    {
        $cmId = $this->getRequest()->getParam('commentary_id');
        $cmModel = $this->commentaryFactory->create()->load($cmId);

        if ($cmModel) {
            return $cmModel;
        }

        return NULL;
    }
}

