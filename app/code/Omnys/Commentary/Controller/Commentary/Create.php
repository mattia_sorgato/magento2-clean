<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Omnys\Commentary\Controller\Commentary;

/**
 * Description of Create
 *
 * @author mattia
 */
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\App\RequestInterface;

/**
 * Commentary creation controller
 */
class Create extends \Magento\Framework\App\Action\Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var \Omnys\Commentary\Helper\Data
     */
    protected $helper;

    /**
     *
     * @var \Omnys\Commentary\Model\CommentaryFactory
     */
    protected $_commentaryFactory;

    /**
     * Index constructor.
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\ForwardFactory $resultForwardFactory
     * @param \Aion\Test\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        \Omnys\Commentary\Model\CommentaryFactory $commentaryFactory,
        \Omnys\Commentary\Helper\Data $helper
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->_commentaryFactory = $commentaryFactory;
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * Dispatch request
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     * @throws \Magento\Framework\Exception\NotFoundException
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->helper->isEnabled()) {
            throw new NotFoundException(__('Page not found.'));
        }
        return parent::dispatch($request);
    }

    /**
     * Omnys Commentary Page
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $cmTitle = trim($this->getRequest()->getParam('title'));
        $cmComment= trim($this->getRequest()->getParam('comment'));
        $cmUsername = trim($this->getRequest()->getParam('username'));
        $cmEmail = trim($this->getRequest()->getParam('email'));

        if (!$this->isFormOk()) {
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('*/');
            return $resultRedirect;
        }

        $cmModel = $this->_commentaryFactory->create();
        $cmModel->setData('title', $cmTitle)
                ->setData('comment', $cmComment)
                ->setData('username', $cmUsername)
                ->setData('email', $cmEmail);

        $cmModel->save();

        $this->getRequest()->setParams(array('commentary_id' => $cmModel->getCommentaryId()));

        /** @var \Magento\Framework\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set(__('Omnys Commentary Creation Page'));
        if (!$resultPage) {
            $resultForward = $this->resultForwardFactory->create();
            return $resultForward->forward('noroute');
        }
        return $resultPage;
    }

    /**
     * Comment form creation input check
     *
     * @return bool
     */
    protected function isFormOk()
    {
        $cmTitle =  $this->getRequest()->getParam('title');
        $cmComment=  $this->getRequest()->getParam('comment');
        $cmUsername =  $this->getRequest()->getParam('username');
        $cmEmail =  $this->getRequest()->getParam('email');

        if (!empty($cmTitle) && !empty($cmComment)
                && !empty($cmUsername) && !empty($cmEmail)) {
            return true;
        }

        return false;
    }
}
